const links = document.querySelectorAll('a');

for (let link of links) {
  link.addEventListener('click', tabHandler);
}

function tabHandler(ev) {
  ev.preventDefault();

  for (let link of links) {
    link.classList.remove('active');
  }
  ev.target.classList.add('active');

  requestForm(ev);
}

function requestForm(ev) {
  const request = new XMLHttpRequest();
  const url = ev ? ev.target.getAttribute('href') : 'components/email-tab.html';

  const preloader = document.querySelector('#preloader')
  request.addEventListener('loadstart', () => {
    preloader.classList.remove('hidden');
  })
  request.addEventListener('loadend', () => {
    preloader.classList.add('hidden');
  })

  request.addEventListener('load', () => {
    if (request.status !== 200) return;
    const targetContentElem = document.querySelector('#content');
    targetContentElem.innerHTML = request.responseText;
  })

  request.open(
    'GET',
    url,
    true
  );
  request.send();
}

requestForm();