const booksContent = document.querySelector('#content');
booksContent.innerHTML = '';

const request = new XMLHttpRequest();

request.addEventListener('load', loadBooks);

function loadBooks() {
  if (request.status !== 200) return;
  const books = JSON.parse(request.responseText);
  books.forEach(book => {
    const bookElem = document.createElement('li');
    bookElem.dataset.title = book.title;
    bookElem.dataset.author = book.author.name;
    bookElem.dataset.info = book.info;
    bookElem.dataset.price = book.price;
    const bookImg = document.createElement('img');
    bookImg.src = book.cover.small;
    bookElem.appendChild(bookImg);
    booksContent.appendChild(bookElem);
  });
}

request.open(
    'GET',
    'https://neto-api.herokuapp.com/book/',
    true
  );
request.send();

  

