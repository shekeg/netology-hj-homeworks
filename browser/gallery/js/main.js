const dir = 'i'
const imagesSrc = [
  `${dir}/breuer-building.jpg`,
  `${dir}/guggenheim-museum.jpg`,
  `${dir}/headquarters.jpg`,
  `${dir}/IAC.jpg`,
  `${dir}/new-museum.jpg`,
];

let i = 0;
currentPhoto.src = imagesSrc[0];
document.getElementById('prevPhoto').onclick = () => {
  i === 0 ? i = imagesSrc.length - 1 : i--;
  currentPhoto.src = imagesSrc[i];
};

document.getElementById('nextPhoto').onclick= () => {
  i < imagesSrc.length - 1 ? i++ : i = 0;
  currentPhoto.src = imagesSrc[i]; 
};