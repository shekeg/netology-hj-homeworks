const dir = 'i';
const imagesSrc = [
  `${dir}/airmax-jump.png`,
  `${dir}/airmax-on-foot.png`,
  `${dir}/airmax-playground.png`,
  `${dir}/airmax-top-view.png`,
  `${dir}/airmax.png`
]

const slider = document.getElementById('slider');

function createSlider(target, imagesSrc) {
  let i = 0;
  slider.src = imagesSrc[0];
  setInterval(() => {
    i === imagesSrc.length - 1 ? i = 0 : i++;
    slider.src = imagesSrc[i];
  }, 5000)
}

createSlider(slider, imagesSrc)