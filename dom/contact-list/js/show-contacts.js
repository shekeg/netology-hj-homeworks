function showContacts(contacts) {
  const contactList = document.querySelector('.contacts-list');
  contactList.innerHTML = '';
  contacts.forEach(({name, email, phone}) => {
    let contactListItem = document.createElement('li');
    contactListItem.dataset.email = email;
    contactListItem.dataset.phone = phone;
    let contactName = document.createElement('strong');
    contactName.innerHTML = name;
    contactListItem.appendChild(contactName);
    contactList.appendChild(contactListItem);
  })
}

const contacts = JSON.parse(loadContacts());

document.addEventListener('DOMContentLoaded', showContacts(contacts));