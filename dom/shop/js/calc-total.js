const cart = {
  count: 0,
  totalPrice: 0
}

function updateCart({count, totalPrice}) {
  document.querySelector('#cart-count').innerHTML = count;
  document.querySelector('#cart-total-price').innerHTML = getPriceFormatted(totalPrice);
}

function addPosition(ev) {
  cart.count++;
  const positionPrice = ev.target.dataset.price;
  cart.totalPrice += +positionPrice;
  updateCart(cart);
}

const buyButtons = document.querySelectorAll('button.add');
for (let buyButton of buyButtons) {
  buyButton.addEventListener('click', addPosition);
}