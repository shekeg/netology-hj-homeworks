'use strict';

const dropdown = document.getElementsByClassName('wrapper-dropdown')[0];
dropdown.addEventListener('click', (ev) => ev.target.classList.toggle('active'));