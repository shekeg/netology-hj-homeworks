'use strict';

const drumKits = document.getElementsByClassName('drum-kit__drum');

for (let drumKit of drumKits) {
  drumKit.addEventListener('click', (ev) => {
    let audio = ev.target.getElementsByTagName('audio')[0];
    audio.pause();
    audio.currentTime = 0;
    audio.play();
  });
};
