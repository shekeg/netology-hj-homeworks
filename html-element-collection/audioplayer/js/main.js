'use strict';

const playList = [
  {
    name: 'LA Chill Tour',
    src: 'https://netology-code.github.io/hj-homeworks/html-element-collection/audioplayer/mp3/LA Chill Tour.mp3'
  },
  {
    name: 'This is it band',
    src: 'https://netology-code.github.io/hj-homeworks/html-element-collection/audioplayer/mp3/This is it band.mp3'
  },
  {
    name: 'LA Fusion Jam',
    src: 'https://netology-code.github.io/hj-homeworks/html-element-collection/audioplayer/mp3/LA Fusion Jam.mp3'
  }
];

const mediaplayer = document.getElementsByClassName('mediaplayer')[0]
const player =   mediaplayer.getElementsByTagName('audio')[0];

const btnBack = document.getElementsByClassName('back')[0];
const btnPlayState = document.getElementsByClassName('playstate')[0];
const btnStop = document.getElementsByClassName('stop')[0];
const btnNext = document.getElementsByClassName('next')[0];

const titlePlayer = document.getElementsByClassName('title')[0];

let i = 0;

titlePlayer.title = playList[i].name;
player.src = playList[i].src;

btnBack.addEventListener('click', () => {
  i === 0 ? i = playList.length - 1 : i--;
  titlePlayer.title = playList[i].name;
  player.src = playList[i].src;
  if (mediaplayer.classList.contains('play')) {
    player.play();
  };
});

btnPlayState.addEventListener('click', () => {
  mediaplayer.classList.toggle('play');
  mediaplayer.classList.contains('play') ? player.play() : player.pause();
});

btnStop.addEventListener('click', () => {
  mediaplayer.classList.remove('play');
  player.pause()
  player.currentTime = 0;
});

btnNext.addEventListener('click', () => {
  i === playList.length - 1 ? i = 0 : i++;
  titlePlayer.title = playList[i].name;
  player.src = playList[i].src;
  if (mediaplayer.classList.contains('play')) {
    player.play();
  };
});