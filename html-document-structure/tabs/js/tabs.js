'use strict';

function init() {
  const tabsContent = document.querySelector('.tabs-content');
  const tabsNav = document.querySelector('.tabs-nav');

  const articles = tabsContent.children;
  const [articlesFirst, ...articlesRest] = articles;
  for (let articleRest of articlesRest) {
    articleRest.classList.add('hidden');
  }

  createTabs();

  const tabLinks = tabsNav.querySelectorAll('li > a');
  tabLinks[0].classList.add('ui-tabs-active');

  for (let tabLink of tabLinks) {
    tabLink.addEventListener('click', selectArticleViaTab);
  }

  function createTabs() {
    let tabClone = tabsNav.querySelector('li').cloneNode(true);
    tabsNav.removeChild(tabsNav.querySelector('li'));
    for (let article of articles) {
      tabClone.querySelector('a').textContent = article.dataset.tabTitle;
      tabClone.querySelector('a').classList.add(article.dataset.tabIcon)
      tabsNav.appendChild(tabClone);
      tabClone = tabsNav.querySelector('li').cloneNode(true);
    }
  }

  function selectArticleViaTab(ev) {
    for (let tabLink of tabLinks) {
      tabLink.parentElement.classList.remove('ui-tabs-active');
      tabLink.classList.remove('ui-tabs-active');
    }
    ev.target.parentElement.classList.add('ui-tabs-active');
    ev.target.classList.add('ui-tabs-active');
    
    for (let article of articles) {
      article.dataset.tabTitle === ev.target.textContent ?
        article.classList.remove('hidden') :
        article.classList.add('hidden');
    }
  }
}

init();