'use strict';

function TodoList(container) {
  const sectionDone = container.querySelector('.done');
  const sectionUndone = container.querySelector('.undone');
  const taskInputs = container.querySelectorAll('input');

  for (let taskInput of taskInputs) {
    taskInput.addEventListener('change', changeTaskStatus);
  }

  function changeTaskStatus(ev) {
    const currentTask = ev.target;

    currentTask.checked ?
      sectionDone.appendChild(currentTask.parentElement) :
      sectionUndone.appendChild(currentTask.parentElement);
  }
}

TodoList(document.querySelector('.todo-list'));

