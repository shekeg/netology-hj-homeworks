'use strict';

function Slider(container) {
  const slides = container.querySelectorAll('.slide');
  let slideCurrent = slides[0];
  slideCurrent.classList.add('slide-current');

  const sliderNav = container.querySelector('.slider-nav');
  sliderNav.addEventListener('click', leafSlide);

  function leafSlide(ev) {
    if (ev.target.classList.contains('disabled')) {
      return;
    }    

    const slideCurrent = container.querySelector('.slide-current');
    const action = ev.target.dataset.action;

    let slideActive;
    if (action === 'first' || action === 'last') {
      slideActive = action === 'last' ?
        slideCurrent.parentElement.lastElementChild : 
        slideCurrent.parentElement.firstElementChild;
    } else {
      slideActive = action === 'next' ?
        slideCurrent.nextElementSibling : 
        slideCurrent.previousElementSibling;
    }

    disableButtons(slideActive);

    slideCurrent.classList.remove('slide-current');
    slideActive.classList.add('slide-current');
  }

  const btnNext = container.querySelector('a[data-action="next"]');
  const btnPrev = container.querySelector('a[data-action="prev"]');
  const btnLast = container.querySelector('a[data-action="last"]');
  const btnFirst = container.querySelector('a[data-action="first"]');
  btnPrev.classList.add('disabled');
  btnFirst.classList.add('disabled');

  function disableButtons(slideActive) {
    if (slideActive.nextElementSibling === null) {
      btnNext.classList.add('disabled');
      btnLast.classList.add('disabled');
    } else {
      btnNext.classList.remove('disabled');
      btnLast.classList.remove('disabled');
    }
    if (slideActive.previousElementSibling === null) {
      btnPrev.classList.add('disabled');
      btnFirst.classList.add('disabled');
    } else {
      btnPrev.classList.remove('disabled');
      btnFirst.classList.remove('disabled');
    }
  }
}

Slider(document.querySelector('.slider'));