'use strict';

const conection = new WebSocket('wss://neto-api.herokuapp.com/counter');

window.addEventListener('beforeunload', () => {
  conection.close(1000);
});

conection.addEventListener('message', renderInfo);

conection.addEventListener('error', () => console.log(error));

function renderInfo(ev) {
  const {connections, errors} = JSON.parse(ev.data);

  const counterElem = document.querySelector('.counter');
  const errorsElem = document.querySelector('.errors');

  counterElem.textContent = connections;
  errorsElem.textContent = errors;
}
