'use strict';

const chatElem = document.querySelector('.chat');

const messageBoxElem = chatElem.querySelector('.message-box');
const messageInputElem = chatElem.querySelector('.message-input');
const messageSubmitElem = chatElem.querySelector('.message-submit');
const messageContentElem = chatElem.querySelector('.messages-content');

const [messageLoadingTemp, messageTemp, messagePersonalTemp, messageStatusTemp] =
  chatElem.querySelectorAll('.message')

const chatStatusElem = chatElem.querySelector('.chat-status');


const conntection = new WebSocket('wss://neto-api.herokuapp.com/chat');

conntection.addEventListener('open', () => {
  chatStatusElem.textContent = chatStatusElem.dataset.online;
  messageSubmitElem.disabled = false;
  const messageStatusElem = messageStatusTemp.cloneNode(true);
  messageStatusElem.querySelector('.message-text').textContent =
    'Пользователь появился в сети';
  messageContentElem.append(messageStatusElem);
});

conntection.addEventListener('close', () => {
  chatStatusElem.textContent = chatStatusElem.dataset.offline;
  messageSubmitElem.disabled = true;
  const messageStatusElem = messageStatusTemp.cloneNode(true);
  messageStatusElem.querySelector('.message-text').textContent =
    'Пользователь не в сети';
  messageContentElem.append(messageStatusElem);
})

conntection.addEventListener('message', ev => {
  const messageText = ev.data;
  const messageLoadingElem = messageLoadingTemp.cloneNode(true);
  if (messageText === '...') {
    messageLoadingElem.querySelector('span').textContent =
      'Собеседник печатает сообщение';
    messageContentElem.append(messageLoadingElem);
  } else if (messageText !== '...') {
    const messageElem = messageTemp.cloneNode(true);
    messageElem.querySelector('.message-text').textContent = messageText;
    messageElem.querySelector('.timestamp').textContent = getCurrentTime();
    if (messageContentElem.contains(messageLoadingElem)) {
      removeChild(messageLoadingElem);
    }
    messageContentElem.append(messageElem);
  }
});

messageBoxElem.addEventListener('submit', ev => {
  ev.preventDefault();
  const textSending = messageInputElem.value;
  conntection.send(textSending);
  messageInputElem.value = '';
  const messagePersonalElem = messagePersonalTemp.cloneNode(true);
  messagePersonalElem.querySelector('.message-text').textContent = textSending;
  messagePersonalElem.querySelector('.timestamp').textContent = getCurrentTime();
  messageContentElem.append(messagePersonalElem);
})

function getCurrentTime() {
  const data = new Date();
  const hours = data.getHours();
  const minutes = data.getMinutes();
  return data.toLocaleTimeString('ru-RU', {hour: '2-digit', minute:'2-digit'})
}