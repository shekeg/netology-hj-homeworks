const contentElem = document.querySelector('#content');
const preloaderElem = document.querySelector('#loader');

const sourceElem = document.querySelector('#source');
const fromElem = document.querySelector('#from');
const toElem = document.querySelector('#to');

const resultElem = document.querySelector('#result');

const request = new XMLHttpRequest();

request.addEventListener('loadstart', () => {
  contentElem.classList.add('hidden');
  preloaderElem.classList.remove('hidden');
});

request.addEventListener('loadend', () => {
  preloaderElem.classList.add('hidden');
  contentElem.classList.remove('hidden');
});


request.addEventListener('load', () => {

  if (request.status !== 200) return;
  const currencys = JSON.parse(request.responseText);
  for (let currency of currencys) {
    let optionElem = document.createElement('option');
    optionElem.value = currency.value;
    optionElem.innerHTML = currency.code;

    let clonedOptionElem = optionElem.cloneNode(true);
    fromElem.appendChild(optionElem);
    toElem.appendChild(clonedOptionElem);
  }
})


sourceElem.addEventListener('input', calcResult);
fromElem.addEventListener('input', calcResult);
toElem.addEventListener('input', calcResult);

function calcResult() {
  resultElem.value = sourceElem.value * fromElem.value / toElem.value;
}

request.open(
  'GET',
  'https://neto-api.herokuapp.com/currency',
  true
)
request.send();