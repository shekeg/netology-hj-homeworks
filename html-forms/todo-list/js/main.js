const listBlockElem = document.querySelector('.list-block');
const tasksElem = listBlockElem.querySelectorAll('input[type="checkbox"]');
const outputElem = listBlockElem.querySelector('output');

function calcTasksCompleted() {
  let tasksCompleted = 0;
  for (let taskElem of tasksElem) {
    if (taskElem.checked === true) {
      tasksCompleted++;
    }
  }
  outputElem.value = `${tasksCompleted} из ${tasksElem.length}`;
  tasksCompleted === tasksElem.length ? listBlockElem.classList.add('complete')
    : listBlockElem.classList.remove('complete');
}

for (let taskElem of tasksElem) {
  taskElem.addEventListener('change', calcTasksCompleted);
}

calcTasksCompleted();