const contentForm = document.querySelector('.contentform');
const fields = document.querySelectorAll('input, textarea');
const outputBlock = document.querySelector('#output');
const submitButton = document.querySelector('.button-contact[type="submit"]');
const editButton = document.querySelector('.button-contact:not([type="submit"])');

for (let field of fields) {
  field.addEventListener('input', validateForm);
}

function validateForm() {
  for (let field of fields) {
    if (contentForm[field.name].value === '') {
      return;
    }
  }
  submitButton.removeAttribute('disabled');
}

document.querySelector('input[name="zip"]').addEventListener('input', (ev) => {
  const {value} = ev.target;
  if (!/^[0-9]+$/.test(value)) { 
    ev.target.value = value.substring(0,value.length-1);
  }
})

submitButton.addEventListener('click', (ev) => {
  ev.preventDefault();
  contentForm.classList.add('hidden');
  outputBlock.classList.remove('hidden');

  outputs = document.querySelectorAll('output');

  for (let output of outputs) {
    output.value = contentForm[output.id].value;
  }
})

editButton.addEventListener('click', () => {
  outputBlock.classList.add('hidden');
  contentForm.classList.remove('hidden');
})