'use strict';

const itemsList = document.querySelector('.items-list');

itemsList.addEventListener('click', ev => {
  if (ev.target.classList.contains('add-to-cart')) {
    ev.preventDefault();
    const item = {
      title: ev.target.dataset.title,
      price: ev.target.dataset.price
    }
    addToCart(item);
  }

})