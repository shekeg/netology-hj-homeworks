'use strict';

function handleTableClick(event) {
  const target = event.target;
  if (target.classList.contains('prop__name')) {
    const field = target.dataset.propName
    event.currentTarget.dataset.sortBy = field;
    const direction = setDirection(target);
    sortTable(field, direction)
  }

  function setDirection(target) {
    if (target.dataset.dir == 1) {
      target.dataset.dir = -1;
      return -1;
    } else {
      target.dataset.dir = 1;
      return 1;
    }
  }
}
