'use strict';

const counter = document.querySelector('#counter');
localStorage.count = localStorage.count || 0;
counter.textContent = localStorage.count;

const wrapBtns = document.querySelector('.wrap-btns');

wrapBtns.addEventListener('click', ev => {
  if (ev.target.id === 'increment')
    handlerIncrement();
  if (ev.target.id === 'decrement')
    handlerDecrement();
  if (ev.target.id === 'reset')
    handlerReset()
})

function handlerIncrement() {
  localStorage.count++;
  counter.textContent = localStorage.count;
}

function handlerDecrement() {
  if (localStorage.count <= 0) {
    return
  }
  localStorage.count--;
  counter.textContent = localStorage.count;
}

function handlerReset() {
  localStorage.count = 0;
  counter.textContent = localStorage.count;
}