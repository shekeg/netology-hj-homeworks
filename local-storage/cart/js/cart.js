'use strict';

const colorSwatch = document.querySelector('#colorSwatch');
const sizeSwatch = document.querySelector('#sizeSwatch');

function getOptions(type) {
  let reqURL = ''
  if (type === 'color') {
    reqURL = 'https://neto-api.herokuapp.com/cart/colors';
  }
  if (type === 'size') {
    reqURL = 'https://neto-api.herokuapp.com/cart/sizes';
  }
  
  return fetch(reqURL)
    .then(res => {
      return res.json()
    })
    .then(items => {
      return createSwatch(items, type);
    })
    .then(createHandlers(type))
    .catch(e => console.log(e));
}

function createSwatch(items, type) {
  let layoutSwatch = '';
  if (type === 'color') {
    layoutSwatch = items.reduce((layoutColorSwatch, color, index) => {
      return layoutColorSwatch + createColorLayoutItem(color, index);
    }, '');
  }
  if (type === 'size') {
    layoutSwatch = items.reduce((layoutSizeSwatch, size, index) => {
      return layoutSizeSwatch + createSizeLayoutItem(size, index);
    }, '');
  }
  insertLayoutSwatch(layoutSwatch, type);
}

function createColorLayoutItem(item, index) {
  return `<div data-value="${item.type}" class="swatch-element color ${item.type} ${item.isAvailable ? 'available' : 'soldout'}">
      <div class="tooltip">${item.title}</div>
      <input quickbeam="color" id="swatch-${index}-${item.type}" type="radio" 
        name="color" value="${item.type}" ${localStorage.color === item.type ? 'checked' : ''} ${item.isAvailable ? '' : 'disabled'}>
      <label for="swatch-${index}-${item.type}" style="border-color: red;">
        <span style="background-color: ${item.code};"></span>
        <img class="crossed-out" src="https://neto-api.herokuapp.com/hj/3.3/cart/soldout.png?10994296540668815886">
      </label>
    </div>`
}

function createSizeLayoutItem(item, index) {
  return `<div data-value="${item.type}" class="swatch-element plain ${item.type} ${item.isAvailable ? 'available' : 'soldout'}">
      <input id="swatch-${index}-${item.type}" type="radio" name="size" 
        value="${item.type}" ${localStorage.size === item.type ? 'checked' : ''} ${item.isAvailable ? '' : 'disabled'}>
      <label for="swatch-${index}-${item.type}">
        ${item.title}
        <img class="crossed-out" src="https://neto-api.herokuapp.com/hj/3.3/cart/soldout.png?10994296540668815886">
      </label>
    </div>`
}

function insertLayoutSwatch(layoutSwatch, type) {
  if (type === 'color') {
    colorSwatch.innerHTML += layoutSwatch;
  }
  if (type === 'size') {
    sizeSwatch.innerHTML += layoutSwatch;
  }
}

function createHandlers(type) {
  let target;
  if (type === 'color') {
    target = colorSwatch;
  }
  if (type === 'size') {
    target = sizeSwatch;
  }

  target.addEventListener('click', ev => {
    if (ev.target.type !== 'radio') {
      return
    }
    if (type === 'color') {
      localStorage.color = ev.target.value;
    }
    if (type === 'size') {
      localStorage.size = ev.target.value;
    }
  })

}

getOptions('color');
getOptions('size');


const quickCart = document.querySelector('#quick-cart');

function getQuickCart() {
  fetch('https://neto-api.herokuapp.com/cart',{
    headers: {
      'Content-Type': 'application/json'
    }
  })
  
  .then(res => res.json())
  
  .then(createQuickCart)
  
  .then(createQuickCartHandlers)
  
  .catch(error => console.log(error));
}

function updateQuickCart() {
  fetch('https://neto-api.herokuapp.com/cart',{
    headers: {
      'Content-Type': 'application/json'
    }
  })
  
  .then(res => res.json())
  
  .then(createQuickCart)
  
  .catch(error => console.log(error));
}

function createQuickCart(items) {
  
  let layoutQuickCart = '';
  layoutQuickCart = items.reduce((layoutCartProduct, item) => {
     return layoutCartProduct + createCartProductLayoutItem(item);
  }, '');

    quickCart.innerHTML = layoutQuickCart;
    quickCart.innerHTML += createCartPayLayout(items);
}

function createCartProductLayoutItem(item) {
  return `<div class="quick-cart-product quick-cart-product-static" id="quick-cart-product-${item.id}" style="opacity: 1;">
      <div class="quick-cart-product-wrap">
        <img src="${item.pic}" title="${item.title}">
        <span class="s1" style="background-color: #000; opacity: .5">$800.00</span>
        <span class="s2"></span>
      </div>
      <span class="count hide fadeUp" id="quick-cart-product-count-${item.id}">${item.quantity}</span>
      <span class="quick-cart-product-remove remove" data-id="${item.id}"></span>
    </div>`
}

function createCartPayLayout(items) {
  
  const priceTotal = items.reduce((priceTotal, item) => {
    return priceTotal + item.price * item.quantity;
  }, 0)
  console.log(priceTotal);
  return `<a id="quick-cart-pay" quickbeam="cart-pay" class="cart-ico ${items.length ? 'open' : ''}">
    <span>
      <strong class="quick-cart-text">Оформить заказ<br></strong>
      <span id="quick-cart-price">$${priceTotal}.00</span>
    </span>
    </a>`
}

function createQuickCartHandlers() {
  quickCart.addEventListener('click', ev => {

    if (ev.target.classList.contains('remove')) {
      console.log(ev.target);
      const reqBody = new FormData();
      reqBody.append('productId', ev.target.dataset.id);
  
      fetch('https://neto-api.herokuapp.com/cart/remove', {
        body: reqBody,
        method: 'POST',
      })
  
        .then(res => res.json())
  
        .then(updateQuickCart())
  
        .catch(error => console.log(error));
    } else {
      return;
    }

  })
}

getQuickCart();

const cartFrom = document.querySelector('#AddToCartForm');
cartFrom.addEventListener('click', addToCart);

function addToCart(ev) {
  if (ev.target.type === 'submit' || ev.target.id === 'AddToCartText') {
    ev.preventDefault();
    const formData = new FormData(ev.currentTarget);
    formData.append('productId', ev.currentTarget.dataset.productId);
    
    fetch('https://neto-api.herokuapp.com/cart', {
      method: 'POST',
      body: formData
    })

      .then(res => res.json())

      .then(data => {
        if (!data.error) {
          updateQuickCart();
        } else {
          console.log(data);
        }
      })

      .catch(e => console.log(e));

  } else {
    return;
  }
}