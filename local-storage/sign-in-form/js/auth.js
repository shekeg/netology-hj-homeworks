'use strict';

const signInHtm = document.querySelector('.sign-in-htm');
const signUpHtm = document.querySelector('.sign-up-htm');

signInHtm.addEventListener('click', ev => {
  if (ev.target.classList.contains('button')) {
    handlerSign(ev);
  }
})

signUpHtm.addEventListener('click', ev => {
  if (ev.target.classList.contains('button')) {
    handlerSign(ev);
  }
})

function handlerSign(ev) {
  ev.preventDefault();
  const formData = new FormData(ev.currentTarget);
  const formObj = {};
  for (var [k, v] of formData) {
    formObj[k] = v;
  }

  let reqURL = '';
  let isSignIn;
  if (ev.currentTarget.classList.contains('sign-in-htm')) {
    reqURL = 'https://neto-api.herokuapp.com/signin';
    isSignIn = true;
  }
  if (ev.currentTarget.classList.contains('sign-up-htm')) {
    reqURL = 'https://neto-api.herokuapp.com/signup';
    isSignIn = false;
  }

  const messageElem = ev.currentTarget.querySelector('.error-message');

  fetch(reqURL, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json' 
    },
    body: JSON.stringify(formObj)
  })

    .then(res => {
      if (res.status >= 200 && res.status < 300) {
        return res.json();
      }
    })

    .then(data => {
      if (!data.error) {
        isSignIn ?
          messageElem.textContent = `Пользователь ${data.name} успешно авторизован` :
          messageElem.textContent = `Пользователь ${data.name} успешно зарегистрирован`;
      } else {
        messageElem.textContent = data.message;
      }
    })
    
    .catch(error => {
      console.log(error);
    })
}