'use strict';

const srcPath =
"https://netology-code.github.io/hj-homeworks/event-object/piano/sounds/";
const srcEnd = ["first", "second", "third", "fourth", "fifth"];
const srcPathEnd = ".mp3";
const set = document.getElementsByClassName("set")[0];
const allBtns = Array.from(set.getElementsByTagName("li"));

function sound(event) {
if (event.shiftKey) {
  set.classList.add("lower");
  set.classList.remove("middle");
  for (let i in allBtns) {
    allBtns[i].getElementsByTagName("audio")[0].src =
      srcPath + "lower/" + srcEnd[i] + srcPathEnd;
  }
}
if (event.altKey) {
  set.classList.add("higher");
  set.classList.remove("middle");
  for (let i in allBtns) {
    allBtns[i].getElementsByTagName("audio")[0].src =
      srcPath + "higher/" + srcEnd[i] + srcPathEnd;
  }
}
}

function defaultFunc(event) {
set.classList.add("middle");
set.classList.remove("lower");
set.classList.remove("higher");
for (let i in allBtns) {
  allBtns[i].getElementsByTagName("audio")[0].src =
    srcPath + "middle/" + srcEnd[i] + srcPathEnd;
}
}

function play(event) {
event.currentTarget.getElementsByTagName("audio")[0].play();
}

for (btn of allBtns) {
btn.addEventListener("click", defaultFunc);
btn.addEventListener("click", sound);
btn.addEventListener("click", play);
}
