'use strict';

let currentGallery = document.getElementsByClassName('gallary-current')[0];
const galleryView = document.getElementsByClassName('gallery-view')[0];
const allA = document.getElementsByTagName('a');
const galleryNav = document.getElementsByClassName("gallery-nav")[0]

function view(event) {
  event.preventDefault();
  galleryView.src = event.currentTarget.href;
};

function select(event) {
  Array.from(galleryNav.getElementsByClassName("gallery-current")).forEach((elem)=>elem.classList.remove('gallery-current'));
  currentGallery = event.currentTarget;
  currentGallery.classList.add('gallery-current');
}


for (let img of allA) {
  img.addEventListener('click', view);
  img.addEventListener('click', select);
};