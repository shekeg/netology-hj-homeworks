'use strict'
const menu = document.getElementsByTagName('nav')[0];
const secret = document.getElementsByClassName('secret')[0];

function openMenu(event) {
  if (( event.ctrlKey )&&( event.altKey )&&( event.code === "KeyT" ))
    menu.classList.toggle("visible");
}

const secretKeys = ["KeyY", "KeyT", "KeyN", "KeyJ", "KeyK", "KeyJ", "KeyU", "KeyB", "KeyZ" ];

let i = 0;
function secretCode(event){
    event.code === secretKeys[i] ? i++ : i = 0;
    if (i === 9) secret.classList.toggle('visible');
}
    
document.addEventListener('keydown', openMenu);
document.addEventListener('keydown', secretCode);