'use strict';

const bioElem = document.querySelector('.bio');
const avatarcontainerElem = document.querySelector('.avatarcontainer');
const dataElem = document.querySelector('.data');

function randFunctionName() {
  return 'callback' + String(Math.random()).slice(-6);
}


function loadData(url) {
  const functionName = randFunctionName();
  return new Promise((resolve, reject) => {
    window[functionName] = resolve;
    const script = document.createElement('script');
    script.src = `${url}?callback=${functionName}`;
    document.body.appendChild(script);
  });
};

loadData('https://neto-api.herokuapp.com/twitter/jsonp/')
  .then(data => {
    bioElem.querySelector('[data-wallpaper]').src = data.wallpaper;
    bioElem.querySelector('[data-username]').textContent = data.username;
    avatarcontainerElem.querySelector('[data-pic]').src = data.pic;
    dataElem.querySelector('[data-tweets]').textContent = data.tweets;
    dataElem.querySelector('[data-followers]').textContent = data.followers;
    dataElem.querySelector('[data-following]').textContent = data.following;
  })
  .catch(error => console.log(error));