'use strict';

function randFunctionName() {
  return 'callback' + String(Math.random()).slice(-6);
}

function loadData(url) {
  const functionName = randFunctionName();
  return new Promise((resolve, reject) => {
    window[functionName] = resolve;
    const script = document.createElement('script');
    script.src = `${url}?callback=${functionName}`;
    document.body.appendChild(script);
  })
}

loadData('https://neto-api.herokuapp.com/food/42')
  .then(recipe => {
    document.querySelector('[data-pic]').style.backgroundImage = `url('${recipe.pic}')`;
    document.querySelector('[data-title]').textContent = recipe.title;
    document.querySelector('[data-ingredients]').textContent = 
      recipe.ingredients.join(', ');
    return loadData('https://neto-api.herokuapp.com/food/42/rating');
  })
  .then(raiting => {
    document.querySelector('[data-rating]').textContent = raiting.rating.toFixed(2);
    document.querySelector('[data-star]').style.width = 
      raiting.rating / 10 * 100 + '%';
    document.querySelector('[data-votes]').textContent = `${raiting.votes} оценок`;
    return loadData('https://neto-api.herokuapp.com/food/42/consumers');
  })
  .then(consumersObj => {
    const counsumersElem = document.querySelector('[data-consumers]');
    consumersObj.consumers.forEach(consumer => {
      counsumersElem.innerHTML += `
        <img src="${consumer.pic}" title="${consumer.name}">
      `;
    })
    counsumersElem.innerHTML += `<span>(+${consumersObj.total-4})</span>`
  });