'use strict';

function randFunctionName() {
  return 'callback' + String(Math.random()).slice(-6);
}

function loadData(url) {
  const functionName = randFunctionName();
  return new Promise((resolve, reject) => {
    window[functionName] = resolve;
    const script = document.createElement('script');
    script.src = `${url}?callback=${functionName}`;
    document.body.appendChild(script);
  })
}

loadData('https://neto-api.herokuapp.com/profile/me')
  .then(profile => {
    document.querySelector('[data-name]').textContent = profile.name;
    document.querySelector('[data-description]').textContent = profile.description;
    document.querySelector('[data-position]').textContent = profile.position;
    document.querySelector('[data-pic]').src = profile.pic;
    return loadData(`https://neto-api.herokuapp.com/profile/${profile.id}/technologies`);
  })
  .then(technologies => {
    technologies.forEach((technologie) => {
      document.querySelector('[data-technologies]').innerHTML += `
        <span class="devicons devicons-${technologie}"></span>
      `
    })
  })
  .then(() => { document.querySelector('.content').style.display = 'initial' })
  .catch(error => console.log(error));